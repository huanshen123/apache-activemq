package cn.kgc.config;

import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.jms.Queue;

/**
 * 配置类
 */
@Configuration
public class ActiveMQConfig {

    public static final String QUEUE_MESSAGE = "my_queue";

    @Bean
    public Queue queue(){
        return new ActiveMQQueue(QUEUE_MESSAGE);
    }
}
