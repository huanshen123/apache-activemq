package cn.kgc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.jms.Queue;

/**
 * @author Rock
 * @create 2020-07-31 13:59
 */
@RestController
public class MQProviderController {
    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;
    @Autowired
    private Queue queue;

    @RequestMapping(value="sendQueueMessage/{msg}",method = RequestMethod.GET)
    public String sendQueueMessage(@PathVariable("msg") String msg){
        jmsMessagingTemplate.convertAndSend(queue,msg);
        return "success";
    }
}
