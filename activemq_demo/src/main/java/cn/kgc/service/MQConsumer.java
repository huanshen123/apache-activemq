package cn.kgc.service;

import cn.kgc.config.ActiveMQConfig;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

/**
 * @author Rock
 * @create 2020-07-31 14:10
 */
@Service
public class MQConsumer {
    //监听消息队列
    @JmsListener(destination = ActiveMQConfig.QUEUE_MESSAGE)
    public void receiveQueue(String message){
        if (null != message){
            System.out.println("接收到消息："+message);
        }
    }
}
